
PFont fontA;
PFont fontB;
int fas=30;
int fbs=255;

PGraphics A;
PGraphics B;

int fade=0;
int fade2=0;

images [] imgs;
ArrayList drawn;


String [] taken= {
};

String present= "";

void setup() {
  size(800, 600);
  fontA = loadFont("50.vlw");
  fontB = loadFont("255.vlw");
  A=createGraphics(width, height, P2D);
  B=createGraphics(width, height, P2D);

  drawn=new ArrayList();
  for (int i=0;i<90;i++) {
    int z=i+1;
    taken = append(taken, z+"");
    drawn.add(new Integer(i));
  }

  imgs=new images[10];
  for (int i=0;i<imgs.length;i++) {
    imgs[i]=new images(i+1);
  }
}

void draw() {
  background(0);
  for (int i=0;i<imgs.length;i++) {
    if (imgs[i].life<10) {
      imgs[i]=new images(i+1);
    }
    imgs[i].draw();
  }
  fill(0, 0, 0);
  rect(0, 0, width, height/1.66);
  A.beginDraw();
  textFont(fontA, fas);
  textAlign(RIGHT);
  int x=fas*2;
  int y=fas;
  for (int i=0;i<taken.length;i+=10) {
    x=fas;
    for (int u=0;u<10;u++) {
      x+=fas/10;
      if (taken[u+i].charAt(0)=='.') {
        fill(255, 255, 50);
      }
      else {
        fill(255, 50, 50);
      }
      text(taken[u+i], x, y);
      x+=int(fas*1.5);
    }
    y+=40;
  }
  A.endDraw();

  B.beginDraw();
  fill(200, 255, 200);
  textFont(fontB, fbs*1.3);
  text(present, width, height/2.5-10);
  fill(200, 255-fade, 200);
  rect(width-width/2.5, height/2.5, width/5, 110);

  fill(200, 200, 255-fade2);
  rect(width-width/2.5+width/5+10, height/2.5, width/6+5, 110);
  textFont(fontA, fas);
  fill(0);
  text("nummer", width-width/2.5+125, height/2.5+60);
    text("reset", width-width/2.5+270, height/2.5+60);

  B.endDraw();
  image(A, 0, 0);
  image(B, 0, 0);


  if (fade>0) {
    fade-=10;
  }
  if (fade2>0) {
    fade2-=10;
  }
}

void mouseReleased() {
  if (mouseX > width-width/2.5  
    && mouseX < width-width/2.5 +width/5
    && mouseY > height/2.5 
    && mouseY < height/2.5 +110) {
    fade=200;
    if (drawn.size()>0) {
      present=randomizer()+"";
    }
  }
  if (mouseX > width-width/2.5+width/5+10  
    && mouseX < width-width/2.5+width/5+10 + width/6+5
    && mouseY > height/2.5 
    && mouseY < height/2.5 +110) {
    fade2=200;
    taken = new String[0];

    present= "";
    A=createGraphics(width, height, P2D);
    B=createGraphics(width, height, P2D);

    drawn=new ArrayList();
    for (int i=0;i<90;i++) {
      int z=i+1;
      taken = append(taken, z+"");
      drawn.add(new Integer(i));
    }
  }
}
int randomizer() {
  int ran=int(random(drawn.size()));
  int myDrawn = (Integer) drawn.get(ran);
  taken[myDrawn]="."+taken[myDrawn];
  drawn.remove(ran);
  return myDrawn+1;
}

class images {
  PImage img;
  int x, y;
  int id;
  int life;
  int xdir;
  int ydir;
  images(int id) {
    xdir=int(random(-1.4, 1.4));
    ydir=int(random(-1.4, 1.4));

    life=int(random(200, 900));
    x=int(random(width));
    y=int(random(height));
    this.id=id;
    img=loadImage(id+".jpg");
    img.resize((int) random(img.width/2, img.width), (int) random(img.height/2, img.height));
  }

  void draw() {
    life--;
    x+=xdir;
    y+=ydir;
    blend(img, 0, 0, img.width, img.height, x, y, img.width, img.height, DIFFERENCE);
    //image(img, x, y);
  }
}

